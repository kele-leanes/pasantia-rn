import React from 'react';
import {View, Text, Image, StyleSheet, Button} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Item = ({item, navigation, button}) => {
  const {name, email, website, id} = item;

  const deleteUser = () => {
    AsyncStorage.removeItem('user');
  };

  const setUser = (user) => {
    AsyncStorage.setItem('user', JSON.stringify(user));
  };

  return (
    <View style={styles.item}>
      <View style={styles.container}>
        <View>
          <Image
            style={styles.image}
            source={require('./../assets/profile.jpg')}
          />
        </View>
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.subtitle}>{email}</Text>
          <Text style={styles.subtitle}>{website}</Text>
        </View>
      </View>
      {button ? (
        <View style={styles.buttonWrapper}>
          <Button
            title={'+ info'}
            onPress={() => navigation.navigate('Detalles', {item})}
          />
          <Button title={'Guardar'} onPress={() => setUser(item)} />
        </View>
      ) : (
        <View style={styles.buttonWrapper}>
          <Button title={'eliminar'} onPress={() => deleteUser()} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 80,
    width: 80,
  },
  item: {
    width: '100%',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  container: {
    flexGrow: 0.5,
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'blue',
  },
  subtitle: {
    color: '#bbb',
    fontSize: 16,
  },
  titleWrapper: {
    width: '80%',
    paddingHorizontal: 10,
  },
  description: {
    marginVertical: 5,
  },
  buttonWrapper: {
    alignSelf: 'flex-end',
    marginVertical: 5,
    flexDirection: 'row',
    width: '50%',
    justifyContent: 'space-around',
  },
});

export default Item;
