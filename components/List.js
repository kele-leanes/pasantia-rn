import React from 'react';
import {FlatList} from 'react-native';
import Item from './Item';

function List({items, navigation}) {
  return (
    <FlatList
      renderItem={({item}) => (
        <Item item={item} navigation={navigation} button />
      )}
      data={items}
      keyExtractor={(item) => String(item.id)}
      initialNumToRender={10}
    />
  );
}

export default List;
