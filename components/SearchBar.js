import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';

const SearchBar = ({searchUser}) => {
  const [input, setInput] = useState('');
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={input}
        onChangeText={(text) => setInput(text)}
      />
      <TouchableOpacity style={styles.button} onPress={() => searchUser(input)}>
        <Text>Buscar</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    width: '80%',
    height: 40,
    borderRadius: 15,
    margin: 5,
  },
  button: {
    justifyContent: 'center',
  },
});

export default SearchBar;
