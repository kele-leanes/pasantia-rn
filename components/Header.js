import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const Header = ({title, showInput}) => {
  return (
    <View style={styles.header}>
      <View>
        <Text style={styles.title}>{title}</Text>
      </View>
      <TouchableOpacity style={styles.appButton} onPress={() => showInput()}>
        <Text style={styles.appTitle}>Buscar Personas</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    backgroundColor: 'lightblue',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  appButton: {
    backgroundColor: 'blue',
    height: 60,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appTitle: {
    color: '#fff',
  },
});

export default Header;
