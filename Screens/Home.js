import 'react-native-gesture-handler';
import React, {useState, useEffect, useMemo} from 'react';
import axios from 'axios';
import {SafeAreaView, ActivityIndicator} from 'react-native';
import Header from '../components/Header';
import List from '../components/List';
import SearchBar from '../components/SearchBar';

const Home = ({navigation}) => {
  const [items, setItems] = useState(null);
  const [input, setInput] = useState('');
  const [showInput, setShowInput] = useState(false);

  const fetchData = () => {
    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then((response) => setItems(response.data))
      .catch((err) => console.log(err));
  };

  useEffect(() => fetchData(), []);

  const _showInput = () => {
    setShowInput(!showInput);
  };

  const searchUser = (input) => {
    if (!items) {
      return null;
    }
    console.log(input);
    return items.filter((user) => user.name.toLowerCase().includes(input));
  };

  const filteredUsers = useMemo(() => searchUser(input), [items, input]);

  return (
    <SafeAreaView>
      <Header title={'FACEBOOK'} showInput={_showInput} />
      {showInput ? <SearchBar searchUser={setInput} /> : null}
      {filteredUsers ? (
        <List items={filteredUsers} navigation={navigation} />
      ) : (
        <ActivityIndicator size={'large'} color={'blue'} />
      )}
    </SafeAreaView>
  );
};

export default Home;
