import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//Screens
import Home from '../screens/Home';
import UserDetails from '../screens/UserDetails';


const Stack = createStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Detalles" component={UserDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;
