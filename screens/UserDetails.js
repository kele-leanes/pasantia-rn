import React from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';

const UserDetails = (props) => {
  const {name, address, email, phone, website} = props.route.params.item;
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={{alignItems: 'center'}}>
          <Image
            style={styles.image}
            source={require('./../assets/profile.jpg')}
          />
          <View>
            <Text style={styles.title}>{name}</Text>
          </View>
        </View>
        <View style={styles.textContainer}>
          <Text>{email}</Text>
          <Text>{phone}</Text>
          <Text>{address.street + address.suite}</Text>
          <Text>{address.city}</Text>
          <Text>{website}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    height: 500,
    padding: 20,
    width: '75%',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    justifyContent: 'space-around',
  },
  image: {
    width: 160,
    height: 160,
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
  },
  textContainer: {
    alignItems: 'center',
    height: 200,
    justifyContent: 'space-around',
  },
});

export default UserDetails;
